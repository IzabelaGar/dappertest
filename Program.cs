﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Dappertest
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var query = "Select NomeProduto from Projeto1";
            var dapper = (await DappertestContext.Database.GetDbConnection()
                .Query<ViewModel>(query))
                .ToList();
        }
    }
}
